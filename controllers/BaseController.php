<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\Language;

/**
 * Site controller
 */
class BaseController extends Controller
{


    public function init(){
        parent::init();
        Language::saveLang();
    }

}

<?php

return [
    'adminEmail' => 'admin@example.com',
    'languages' => [
        [
            'id' => 1,
            'url' => 'ua',
            'locale' => 'ua',
            'name' => 'Українська',
            'default' => true,
        ],
        [
            'id' => 2,
            'url' => 'ru',
            'locale' => 'ru',
            'name' => 'Русский',
        ],
    ],
];
